(function () {
    "use strict";
    angular.module("MyApp").controller("ProductsCtrl", ProductsCtrl);

    ProductsCtrl.$inject = ["MyAppService", "$state"];

    function ProductsCtrl(MyAppService, $state) {
        var vm = this; // vm
        vm.products = [];
        vm.showResults = false;
        vm.count = 0;
        vm.selected = "";
        vm.term = "";
        vm.message = "";
        vm.showMessage = false;
        vm.numperpage = 20;

        //search params
        vm.search = {
            limit: 20,
            offset: 0,
            type: "",
            term: "",
            order: "asc",
            orderby: "name"
        }

        //search and display initial results
        vm.searchProducts = function () {
            //set default search params
            // vm.search.limit = 20; //prevent overriding select option
            vm.search.offset = 0;
            vm.search.type = vm.selected;
            vm.search.term = "%" + vm.term + "%";
            vm.search.order = "asc";
            vm.search.orderby = vm.selected == "" ? "name" : vm.selected;

            vm.showResults = false;
            vm.showMessage = false;

            MyAppService.searchProducts(vm.search)
                .then(function (result) {
                    // console.log(result);
                    vm.count = result.n;
                    vm.products = result.data;
                    vm.showResults = true;
                }).catch(function (err) {
                    console.log(err);
                    if (err.status == 404) {
                        vm.message = "No products found";
                        vm.showMessage = true;
                    }
                });
        }


        //upon click of the add button
        vm.addProduct = function () {
            $state.go("add");
        }

        //upon click of the edit button
        vm.editProduct = function (id) {
            // console.log(id);
            $state.go("edit", { id: id });
        }

        //upon first load of page
        vm.init = function () {
            //search params configured to show all products
            vm.showResults = false;
            vm.search.limit = 20;
            vm.search.offset = 0;
            vm.search.type = "name";
            vm.search.term = "%%"; //for all
            vm.search.order = "asc";
            vm.search.orderby = "name";
            vm.numperpage = "20";

            MyAppService.searchProducts(vm.search)
                .then(function (result) {
                    // console.log(result);
                    vm.count = result.n;
                    vm.products = result.data;
                    vm.showResults = true;
                }).catch(function (err) {
                    console.log(err);
                });
        }

        vm.init(); //load page

        //upon select option
        vm.refresh = function () {
            console.log("refresh");
            vm.search.offset = 0;
            vm.search.limit = parseInt(vm.numperpage);
            // console.log(vm.search);
            MyAppService.searchProducts(vm.search)
                .then(function (result) {
                    vm.products = vm.search.offset == 0 ? result.data : result;
                    vm.showResults = true;
                }).catch(function (err) {
                    console.log(err);
                });
        }


        //upon click of the next button
        vm.loadNext = function () {
            vm.search.offset += vm.search.limit;
            // console.log(vm.search);
            MyAppService.searchProducts(vm.search)
                .then(function (result) {
                    vm.products = result;
                    vm.showResults = true;
                }).catch(function (err) {
                    console.log(err);
                });
        }

        //upon click of the previous button
        vm.loadPrevious = function () {
            vm.search.offset -= vm.search.limit;

            MyAppService.searchProducts(vm.search)
                .then(function (result) {
                    // console.log(result);
                    vm.products = vm.search.offset == 0 ? result.data : result;
                    vm.showResults = true;
                }).catch(function (err) {
                    console.log(err);
                });
        }

        vm.sortBy = function (propertyName) {
            // console.log("sortBy " + propertyName);
            vm.search.order = (vm.search.orderby == propertyName) ? (vm.search.order == "asc" ? "desc" : "asc") : "asc";

            // vm.search.type = propertyName;
            vm.search.orderby = propertyName;

            //use previous search params
            MyAppService.searchProducts(vm.search)
                .then(function (result) {
                    // console.log(result);
                    vm.products = vm.search.offset == 0 ? result.data : result;
                    vm.showResults = true;
                }).catch(function (err) {
                    console.log(err);
                    if (err.status == 404) {
                        vm.message = "No products found";
                        vm.showMessage = true;
                    }
                });
        }


        vm.getFirstNum = function () {
            return vm.search.offset + 1;
        }

        vm.getNextNum = function () {
            return vm.search.offset + vm.search.limit < vm.count ? vm.search.offset + vm.search.limit : vm.count;
        }

        vm.getDisplayResultsMessage = function () {
            var msg = "Displaying ";
            if (vm.count > vm.search.limit) {
                msg += "" + vm.getFirstNum() + " to " + vm.getNextNum() + " of ";
            }
            msg += "" + vm.count + " result";
            if (vm.count > 1) {
                msg += "s";
            }

            return msg;
        }

        vm.hidePrevButton = function() {
            return vm.search.offset == 0;
        }

        vm.showNextButton = function() {
            return vm.search.offset + vm.search.limit < vm.count;
        }

        vm.getPrevButtonText = function() {
            return "Previous " + vm.search.limit;
        }

        vm.getNextButtonText = function() {
            return "Next " + (vm.search.offset+2*vm.search.limit < vm.count ? vm.search.limit : (vm.count - vm.search.limit - vm.search.offset));
        }
    }

})();