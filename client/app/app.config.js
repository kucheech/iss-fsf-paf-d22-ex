(function () {
    "use strict";
    angular.module("MyApp").config(MyConfig);
    MyConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function MyConfig($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state("products", {
                url: "/products",
                templateUrl: "/views/products.html",
                controller: "ProductsCtrl as ctrl"
            })
            .state("edit", {
                url: "/edit/:id",
                templateUrl: "/views/edit.html",
                controller: "EditProductCtrl as ctrl"
            })
            .state("add", {
                url: "/add",
                templateUrl: "/views/add.html",
                controller: "AddProductCtrl as ctrl"
            })

        $urlRouterProvider.otherwise("products");
    }





})();