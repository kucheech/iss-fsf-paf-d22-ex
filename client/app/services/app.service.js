(function () {
    angular.module("MyApp").service("MyAppService", MyAppService);

    MyAppService.$inject = ["$http", "$q", "Upload"];

    function MyAppService($http, $q, Upload) {
        var service = this;

        //expose the following services
        service.searchProducts = searchProducts;
        service.getProductById = getProductById;
        service.addProduct = addProduct;
        service.updateProduct = updateProduct;
        service.deleteProductById = deleteProductById;
        service.uploadImage = uploadImage;

        function uploadImage(file, upc12) {
            console.log("uploadImage");
            var defer = $q.defer();
            Upload.upload({
                url: "/upload/" + upc12,
                data: {
                    "img-file": file
                }
            }).then(function (result) {
                console.log(result);
                defer.resolve(result.data);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });
            
            return defer.promise;
        }

        function searchProducts(search) {
            var defer = $q.defer();
            $http.post("/products/search", { search: search })
                .then(function (result) {
                    // console.log(result);
                    defer.resolve(result.data);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }

        function addProduct(product) {
            var defer = $q.defer();
            const id = product.id;
            $http.post("/product/", { product: product })
                .then(function (result) {
                    // console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }

        function updateProduct(product) {
            var defer = $q.defer();
            const id = product.id;
            $http.put("/product/" + id, { product: product })
                .then(function (result) {
                    // console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }

        function getProductById(id) {
            var defer = $q.defer();

            $http.get("/product/" + id).then(function (result) {
                // console.log(result);
                if (result.status == 200) {
                    defer.resolve(result.data);
                } else {
                    defer.resolve(null);
                }

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        function deleteProductById(id) {
            var defer = $q.defer();

            $http.delete("/product/" + id).then(function (result) {
                // console.log(result);
                if (result.status == 202) {
                    defer.resolve(result.data);
                } else {
                    defer.resolve(null);
                }

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

    }

})();