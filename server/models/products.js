module.exports = function (sequelize, Sequelize) {
    const Products = sequelize.define("grocery_list",
        {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            upc12: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            brand: {
                type: Sequelize.STRING,
                allowNull: false
            },
            name: {
                type: Sequelize.STRING,
                allowNull: false
            }
        }, {
            tableName: "grocery_list",
            // don't add timestamps attributes updatedAt and createdAt
            timestamps: false
            // disable the modification of table names; By default, sequelize will automatically
            // transform all passed model names (first parameter of define) into plural.
            // if you don't want that, set the following
            // , freezeTableName: true
        });

    return Products;
};