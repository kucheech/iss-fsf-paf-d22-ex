## Exercise on sequelize
to use Assessment02 codebase

### config
```
const DB_NAME = "iss-fsf";
const DB_USER = "root";
const DB_PASSWORD = "password";
const DB_HOST = "localhost"
const Sequelize = require("sequelize");
const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASSWORD,
    {
        host: DB_HOST,
        dialect: 'mysql',
        logging: console.log,
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        define: {
            timestamps: false
        }
    }
);
const Products = require("./models/products")(sequelize, Sequelize);
```

### Model
```
module.exports = function (sequelize, Sequelize) {
    const Products = sequelize.define("grocery_list",
        {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            upc12: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            brand: {
                type: Sequelize.STRING,
                allowNull: false
            },
            name: {
                type: Sequelize.STRING,
                allowNull: false
            }
        }, {
            tableName: "grocery_list",
            timestamps: false
        });

    return Products;
};
```

### functions used
* findAll
* findAndCountAll
* findById
* findOrCreate
* create
* update
* destroy

### my most complex sequelize function (in search)
```
        Products.findAndCountAll({
            where: sequelize.where(sequelize.col(search.type), { $like: search.term }),
            order: [[search.orderby, search.order]],
            limit: search.limit,
            offset: 0
        }).then(result => {
            if (result.count > 0) {
                var data = {
                    n: result.count,
                    data: result.rows
                };
                res.status(200).json(data);
            } else {
                res.status(404).send("No products found");
            }
        }).catch(err => {
            handleError(err, res);
        });
```